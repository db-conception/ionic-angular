import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  private recipes : Recipe[] = [
    {
      id: 'r1',
      imgUrl : "https://www.adeline-cuisine.fr/wp-content/uploads/2016/08/salade-saumon-fume-avocat-tomate-oignon-ete-recette-678x508.jpg",
      ingredients : ["salad", "parmesan", "salmon", "french fries"],
      title : "salmon salad"
    } as Recipe,
    {
      id: 'r2',
      imgUrl : "https://assets.afcdn.com/recipe/20181017/82766_w600.jpg",
      ingredients : ["agneau", "patate", "carotte", "oignon", "olives"],
      title : "tajine d'agneau"
    } as Recipe
  ];
  
  constructor() { }

  getallRecipe(){
    return [...this.recipes]; // return a copy of the array
  }

  getRecipe(id : string){
    return { ...this.recipes.find(r => r.id === id) };
  }

  deleteRecipe(id : string) {
    this.recipes = [...this.recipes.filter(r => r.id !== id)];
  }
}
